import ipaddress

from django.template import engines

import copy

from django_cdstack_deploy.django_cdstack_deploy.func import (
    generate_config_static,
    zip_add_file,
)
from django_cdstack_tpl_deb.django_cdstack_tpl_deb.views import handle as handle_deb
from django_cdstack_tpl_deb.django_cdstack_tpl_deb.views import get_os_release
from django_cdstack_tpl_deb_stretch.django_cdstack_tpl_deb_stretch.views import (
    get_apt_packages,
    get_apt_sources,
    get_apt_prefs,
)
from django_cdstack_tpl_deb_buster.django_cdstack_tpl_deb_buster.views import (
    handle as handle_deb_buster,
)
from django_cdstack_tpl_dnslb.django_cdstack_tpl_dnslb.views import (
    handle as handle_dnslb,
    merge_dnsdist_resolver,
)
from django_cdstack_tpl_crowdsec_main.django_cdstack_tpl_crowdsec_main.views import (
    handle as handle_crowdsec_main,
)
from django_cdstack_tpl_fail2ban.django_cdstack_tpl_fail2ban.views import (
    handle as handle_fail2ban,
)
from django_cdstack_tpl_wazuh_agent.django_cdstack_tpl_wazuh_agent.views import (
    handle as handle_wazuh_agent,
)
from django_cdstack_tpl_ipcop.django_cdstack_tpl_ipcop.views import (
    handle as handle_ipcop,
)
from django_cdstack_tpl_keadhcp.django_cdstack_tpl_keadhcp.views import (
    handle as handle_keadhcp,
)
from django_cdstack_tpl_whclient.django_cdstack_tpl_whclient.views import (
    handle as handle_whclient,
)


def handle(zipfile_handler, template_opts, cmdb_host, skip_handle_os=False):
    django_engine = engines["django"]

    module_prefix = "django_cdstack_tpl_guestgw_v2/django_cdstack_tpl_guestgw_v2"

    if "apt_packages" not in template_opts:
        template_opts["apt_packages"] = get_apt_packages(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if "fail2ban_apps" not in template_opts:
        template_opts["fail2ban_apps"] = copy.deepcopy(template_opts["apt_packages"])

    if "apt_sources" not in template_opts:
        template_opts["apt_sources"] = get_apt_sources(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if "apt_preferences" not in template_opts:
        template_opts["apt_preferences"] = get_apt_prefs(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    if "os_release" not in template_opts:
        template_opts["os_release"] = get_os_release(
            module_prefix + "/templates/image/imageconf/vmtemplate.yml"
        )

    dnsdist_resolver_list = merge_dnsdist_resolver(template_opts)

    template_opts = {**template_opts, **dnsdist_resolver_list}

    mesh_network_profile_list = list()
    mesh_network_fastd_profile_list = list()
    mesh_network_tunneldigger_profile_list = list()

    for key in template_opts.keys():
        if key.startswith("mesh_network_prof_") and key.endswith("_name"):
            key_ident = key[:-5]

            mesh_network_profile = dict()
            mesh_network_profile["network_domain_name"] = template_opts[
                key_ident + "_name"
            ]
            mesh_network_profile["mesh_network_router"] = template_opts[
                key_ident + "_router"
            ]
            mesh_network_profile["mesh_network_bridge_id"] = template_opts[
                key_ident + "_bridge_id"
            ]

            mesh_network_profile["mesh_network_bridge_device"] = (
                "ovsbr-" + mesh_network_profile["mesh_network_bridge_id"] + "-clt"
            )

            mesh_network_profile["own_ip"] = template_opts[
                "network_iface_"
                + mesh_network_profile["mesh_network_bridge_device"]
                + "_ip"
            ]
            mesh_network_profile["own_ip6"] = template_opts[
                "network_iface_"
                + mesh_network_profile["mesh_network_bridge_device"]
                + "_ip6"
            ]

            net_cidr4 = ipaddress.ip_network(
                mesh_network_profile["own_ip"]
                + "/"
                + template_opts[
                    "network_iface_"
                    + mesh_network_profile["mesh_network_bridge_device"]
                    + "_netmask"
                ],
                strict=False,
            )
            net_cidr4_hosts = list(net_cidr4.hosts())

            net_cidr6 = ipaddress.ip_network(
                mesh_network_profile["own_ip6"]
                + "/"
                + template_opts[
                    "network_iface_"
                    + mesh_network_profile["mesh_network_bridge_device"]
                    + "_netmask6"
                ],
                strict=False,
            )

            mesh_network_profile["dhcp_scope"] = str(net_cidr4)
            mesh_network_profile["dhcp_broadcast"] = str(net_cidr4.broadcast_address)
            mesh_network_profile["dhcp_netmask"] = str(net_cidr4.netmask)
            mesh_network_profile["dhcp_network"] = str(net_cidr4.network_address)
            mesh_network_profile["dhcp_scope_first"] = str(net_cidr4_hosts[50])
            mesh_network_profile["dhcp_scope_last"] = str(net_cidr4_hosts[-1])
            mesh_network_profile["radvd_prefix"] = str(net_cidr6)

            if key_ident + "_tunneldigger_enabled" in template_opts.keys():
                if template_opts[key_ident + "_tunneldigger_enabled"] == "true":
                    mesh_network_profile["tunneldigger_port"] = template_opts[
                        key_ident + "_tunneldigger_port"
                    ]
                    mesh_network_profile["tunneldigger_port_base"] = template_opts[
                        key_ident + "_tunneldigger_port_base"
                    ]

                    merged_opts = {**template_opts, **mesh_network_profile}

                    config_template_file = open(
                        module_prefix
                        + "/templates/config-fs/dynamic/etc/tunneldigger/broker/tunneldigger.conf",
                        "r",
                    ).read()
                    config_template = django_engine.from_string(config_template_file)

                    zip_add_file(
                        zipfile_handler,
                        "etc/tunneldigger/broker/"
                        + mesh_network_profile["network_domain_name"]
                        + ".conf",
                        config_template.render(merged_opts),
                    )

                    config_template_file = open(
                        module_prefix
                        + "/templates/config-fs/dynamic/etc/tunneldigger/broker/scripts/session-up.sh",
                        "r",
                    ).read()
                    config_template = django_engine.from_string(config_template_file)

                    zip_add_file(
                        zipfile_handler,
                        "etc/tunneldigger/broker/"
                        + mesh_network_profile["network_domain_name"]
                        + "/session-up.sh",
                        config_template.render(merged_opts),
                    )

                    config_template_file = open(
                        module_prefix
                        + "/templates/config-fs/dynamic/etc/tunneldigger/broker/scripts/session-down.sh",
                        "r",
                    ).read()
                    config_template = django_engine.from_string(config_template_file)

                    zip_add_file(
                        zipfile_handler,
                        "etc/tunneldigger/broker/"
                        + mesh_network_profile["network_domain_name"]
                        + "/session-down.sh",
                        config_template.render(merged_opts),
                    )

                    mesh_network_tunneldigger_profile_list.append(mesh_network_profile)

            if key_ident + "_fastd_enabled" in template_opts.keys():
                if template_opts[key_ident + "_fastd_enabled"] == "true":
                    mesh_network_profile["fastd_port"] = template_opts[
                        key_ident + "_fastd_port"
                    ]
                    mesh_network_profile["fastd_private_key"] = template_opts[
                        key_ident + "_fastd_private_key"
                    ]

                    merged_opts = {**template_opts, **mesh_network_profile}

                    config_template_file = open(
                        module_prefix
                        + "/templates/config-fs/dynamic/etc/fastd/connection/fastd.conf",
                        "r",
                    ).read()
                    config_template = django_engine.from_string(config_template_file)

                    zip_add_file(
                        zipfile_handler,
                        "etc/fastd/"
                        + mesh_network_profile["network_domain_name"]
                        + "/fastd.conf",
                        config_template.render(merged_opts),
                    )

                    config_template_file = open(
                        module_prefix
                        + "/templates/config-fs/dynamic/etc/fastd/connection/secret.conf",
                        "r",
                    ).read()
                    config_template = django_engine.from_string(config_template_file)

                    zip_add_file(
                        zipfile_handler,
                        "etc/fastd/"
                        + mesh_network_profile["network_domain_name"]
                        + "/secret.conf",
                        config_template.render(merged_opts),
                    )

                    mesh_network_fastd_profile_list.append(mesh_network_profile)

            mesh_network_profile_list.append(mesh_network_profile)

    template_opts["mesh_network_tunneldigger_profile_list"] = (
        mesh_network_tunneldigger_profile_list
    )
    template_opts["mesh_network_fastd_profile_list"] = mesh_network_fastd_profile_list
    template_opts["mesh_network_profile_list"] = mesh_network_profile_list

    generate_config_static(zipfile_handler, template_opts, module_prefix)
    handle_keadhcp(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_dnslb(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_ipcop(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_fail2ban(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_crowdsec_main(zipfile_handler, template_opts, cmdb_host, skip_handle_os)
    handle_wazuh_agent(zipfile_handler, template_opts, cmdb_host, skip_handle_os)

    if not skip_handle_os:
        handle_deb(zipfile_handler, template_opts, cmdb_host, skip_handle_os)

    return True
