#!/bin/bash
INTERFACE="$3"

ip link set dev "$INTERFACE" up mtu 1364
brctl addif br-{{ mesh_network_bridge_id }}-mesh "$INTERFACE"
#ovs-vsctl add-port ovsbr-{{ mesh_network_bridge_id }}-mesh "$INTERFACE"
