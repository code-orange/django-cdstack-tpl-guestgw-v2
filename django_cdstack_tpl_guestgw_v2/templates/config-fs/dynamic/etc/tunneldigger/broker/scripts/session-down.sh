#!/bin/bash
INTERFACE="$3"

brctl delif br-{{ mesh_network_bridge_id }}-mesh "$INTERFACE"
#ovs-vsctl del-port ovsbr-{{ mesh_network_bridge_id }}-mesh "$INTERFACE"
